package com.programming.reactive.demo.user;

import org.springframework.context.ApplicationEvent;

public class UserCreatedEvent extends ApplicationEvent {

    public UserCreatedEvent(User source) {
        super(source);
    }
}
