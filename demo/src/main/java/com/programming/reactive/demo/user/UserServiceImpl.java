package com.programming.reactive.demo.user;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ApplicationEventPublisher publisher;

    @Override
    public Mono<User> update(String id, User user) {
        return this.userRepository
                .findById(id)
                .map(p -> new User(p.getId(), user.getUsername(), user.getEmail()))
                .flatMap(this.userRepository::save);
    }

    @Override
    public Mono<User> save(User user) {
        log.info("Saving the user to the database " + user);
        return this.userRepository
                .save(user)
                .doOnSuccess(newUser -> this.publisher.publishEvent(new UserCreatedEvent(newUser)));
    }

    @Override
    public Mono<User> delete(String userId) {
        return this.userRepository
                .findById(userId)
                .flatMap(o -> this.userRepository.deleteById(o.getId()).thenReturn(o));
    }

    @Override
    public Mono<User> getById(String userId) {
        return this.userRepository
                .findById(userId);
    }

    @Override
    public Flux<User> getAll() {
        return this.userRepository
                .findAll();
    }
}
