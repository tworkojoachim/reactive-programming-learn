package com.programming.reactive.demo.user;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicate;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class UserEndpointConfiguration {

    private final static String PATH_ID = "/users/{id}";
    private final static String PATH = "/users";

    @Bean
    RouterFunction<ServerResponse> routes(UserHandler handler) {
        return route(i(GET(PATH)), handler::all)
                .andRoute(i(GET(PATH_ID)), handler::getById)
                .andRoute(i(PUT(PATH_ID)), handler::updateById)
                .andRoute(i(DELETE(PATH_ID)), handler::deleteById)
                .andRoute(i(POST(PATH)), handler::create);
    }

    private static RequestPredicate i(RequestPredicate target) {
        return new CaseInsensitiveRequestPredicate(target);
    }
}
