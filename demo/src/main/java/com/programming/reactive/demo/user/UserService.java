package com.programming.reactive.demo.user;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

    Mono<User> update(String id, User user);

    Mono<User> save(User user);

    Mono<User> delete(String userId);

    Mono<User> getById(String userId);

    Flux<User> getAll();
}
